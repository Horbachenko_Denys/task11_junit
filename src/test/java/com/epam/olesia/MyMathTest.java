package com.epam.olesia;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class MyMathTest {

   static MyMath myMath;

    @BeforeAll
            public static void init() {
        myMath = new MyMath();
    }


    @Test
    public void testSum() {
        myMath = new MyMath();
        int result = myMath.sum(5,5);
        assertEquals(10, result);
    }

    @Test
    public void testMinus() {
        myMath = new MyMath();
        int result = myMath.minus(5,5);
        assertEquals(0, result);
    }

    @AfterAll
    public static void close() {
        //Close here all connections/resources.
    }
}
