package com.epam.olesia;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DatabaseConnectorTest {

    @Test
    public void testGetDataById () {
       DatabaseConnector databaseConnectorMock = mock(DatabaseConnector.class);
       when(databaseConnectorMock.getDataByid(0)).thenReturn("some response");
       String expected = "some response";
        assertEquals(databaseConnectorMock.getDataByid(0), expected);
    }
    @Test
    public void testRealGetDataById () {
        DatabaseConnector databaseConnector = new DatabaseConnector();
        String result = databaseConnector.getDataByid(0);
        String expected = "Some data response with id: 0";
        assertEquals(result, expected);
    }
}
